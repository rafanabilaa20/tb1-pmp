#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pm4py as pm
import pandas as pd


# In[2]:


data = pd.read_csv("C:/Users/ASUS/Documents/pm/running-example-exported.csv", sep=',')
data = data[['case_id','activity','timestamp']]


# In[3]:


data = data.rename(columns={'case_id':'case:concept:name','activity':'concept:name','timestamp':'time:timestamp'})
start = pm.get_start_activities(data)
df_start_activities = pm.filter_start_activities(data,['register request']) 


# In[6]:


df_start_activities


# In[7]:


end_avtivities = pm.get_end_activities(df_start_activities)
df_filter_end = pm.filter_end_activities(df_start_activities, end_avtivities)


# In[8]:


end_avtivities


# In[9]:


df_filter_end


# In[10]:


from pm4py.objects.conversion.log import converter as log_converter
log = log_converter.apply(df_filter_end)
log


# In[11]:


from pm4py.algo.discovery.alpha import algorithm as alpha_miner
net, initial_marking, final_marking = alpha_miner.apply(log)


# In[12]:


from pm4py.visualization.petri_net import visualizer as pn_visualizer
gviz = pn_visualizer.apply(net, initial_marking, final_marking)
pn_visualizer.save(gviz,'pn.png')
gviz






